﻿using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;

namespace UnityNSPConvertor
{
    class Program
    {
        //0.4 - removeaccessible url structure
        //0.3 - check if exists in NSP legal info
        //0.2 - remove legal info from NCA
        //0.1 - basic implementation
        private static string kVersion = "0.4";
        private static string kNCADirectory = "NCA";
        private static string kNCAOutputDirectory = "NCA_out";
        private static string kNSPOutputDirectory = "NSP_out";
        private static string kTitleLisFile = kNCADirectory + @"/titlelist.xml";

        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Log($"UnityNSPConverter {kVersion} Missing parametres!");
                Log("UnityNSPConverter.exe AuthoringToolPath HacPacPath NSPFileName TitleId");
                Environment.Exit(1);
            }
            string authoringTool = args[0];
            string hacPac = args[1];
            string nspFileName = args[2];
            string titleId = args[3];

            CheckFileExists(authoringTool);
            CheckFileExists(hacPac);
            CheckFileExists(nspFileName);

            Log($"UnityNSPConverter {kVersion} converting: {nspFileName}");

            //delete previous working directory
            DeleteWorkingDirectory(kNCADirectory);
            DeleteWorkingDirectory(kNCAOutputDirectory);
            DeleteWorkingDirectory(kNSPOutputDirectory);

            //expand NSP
            RunProcess(authoringTool, $"extract {nspFileName} -o {kNCADirectory}");

            //Console.ReadKey();

            //remove legal info which avoid installation with Awoo Installer
            RemoveLegalInfo();

            //remove legal info which avoid installation with Awoo Installer
            RemoveAccessibleUrls();

            //get program and controll NCA directory name
            GetNCAProgramAndControlDirectory(kNCADirectory, out string programDirectoryName, out string controlDirectory);

            //NCA program repack
            RunProcess(hacPac, $"-o .\\{kNCAOutputDirectory} --type nca --ncatype program --exefsdir {programDirectoryName}\\fs0 --romfsdir {programDirectoryName}\\fs1 --logodir {programDirectoryName}\\fs2 --titleid {titleId}");
            string programNCAFileName = GetProgramNCAFileName(kNCAOutputDirectory);

            //NCA controll repack
            RunProcess(hacPac, $"-o .\\{kNCAOutputDirectory} --type nca --ncatype control --romfsdir {controlDirectory}\\fs0 --titleid {titleId}");
            string controlNCAFileName = GetControlNCAFileName(kNCAOutputDirectory, programNCAFileName);

            //generate CNMT
            RunProcess(hacPac, $"-o .\\{kNCAOutputDirectory} --type nca --ncatype meta --titletype application --programnca .\\{programNCAFileName} --controlnca .\\{controlNCAFileName} --titleid {titleId}");

            //build final NSP
            RunProcess(hacPac, $"-o .\\{kNSPOutputDirectory} --type nsp --ncadir .\\{kNCAOutputDirectory} --titleid {titleId}");

            //result
            Log($"You can find result NSP file in:{Directory.GetCurrentDirectory()}\\{kNSPOutputDirectory}");
        }

        static void RunProcess(string fileName, string arguments)
        {
            Log($"RunProcess:{fileName} {arguments}");

            using (Process process = new Process())
            {
                process.StartInfo.FileName = fileName;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                process.WaitForExit();

                Log($"Process output:\n{output}");
            }
        }

        private static void RemoveLegalInfo()
        {
            Log($"RemoveLegalInfo");

            string cnmtFileName = string.Empty;
            string legalInfoFileName = string.Empty;
            GetLegalInfoFileNamedAndRemoveItFromTitleList(ref cnmtFileName, ref legalInfoFileName);

            //check if exists in NSP legal info
            if (!string.IsNullOrEmpty(legalInfoFileName))
            {
                //delete directory with legal content
                string legalInfoDirectory = legalInfoFileName.Replace("legalinfo.xml", "nca");
                legalInfoDirectory = Path.Combine(kNCADirectory, legalInfoDirectory);

                if (Directory.Exists(legalInfoDirectory))
                {
                    Directory.Delete(legalInfoDirectory, true);
                }

                //delete legal info file
                legalInfoFileName = Path.Combine(kNCADirectory, legalInfoFileName);
                if (File.Exists(legalInfoFileName))
                {
                    File.Delete(legalInfoFileName);
                }

                RemoveLegalInfoFromFile(cnmtFileName);
            }
        }

        private static void RemoveAccessibleUrls()
        {
            Log($"RemoveAccessibleUrls");

            //search for file accessible-urls.txt
            string[] files = Directory.GetFiles(kNCADirectory, "accessible-urls.txt", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                int endIndex = file.IndexOf("\\fs0", kNCADirectory.Length + 1);
                if (endIndex > -1)
                {
                    string directoryToDelete = file.Substring(0, endIndex);

                    Log($"Deleting structure for file:{file} directory:{directoryToDelete}");

                    Directory.Delete(directoryToDelete, true);
                }
            }
        }

        private static void GetLegalInfoFileNamedAndRemoveItFromTitleList(ref string cnmtFileName, ref string legalInfoFileName)
        {
            List<string> fileContent = new List<string>();

            //read title list xml
            using (StreamReader streamReader = new StreamReader(kTitleLisFile))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();

                    //it is line with legal info
                    if (line.Contains("legalinfo.xml"))
                    {
                        legalInfoFileName = GetLineContent(line);//get info ad skip
                    }
                    else
                    {
                        if (line.Contains("cnmt.xml"))
                        {
                            cnmtFileName = GetLineContent(line);
                        }

                        fileContent.Add(line);//store for write back
                    }
                }
            }

            if (fileContent.Count > 0)
            {
                string fileContentAsString = string.Join(Environment.NewLine, fileContent.ToArray());
                using (StreamWriter streamWriter = new StreamWriter(kTitleLisFile))
                {
                    streamWriter.Write(fileContentAsString);
                }
            }
        }

        private static string GetLineContent(string line)
        {
            int beginIndex = line.IndexOf(">") + 1;
            int endIdex = line.LastIndexOf("<");
            return line.Substring(beginIndex, endIdex - beginIndex);
        }

        private static void RemoveLegalInfoFromFile(string cnmtFileName)
        {
            cnmtFileName = Path.Combine(kNCADirectory, cnmtFileName);

            if (File.Exists(cnmtFileName))
            {
                bool ignoreLines = false;
                List<string> fileContent = new List<string>();

                using (StreamReader streamReader = new StreamReader(cnmtFileName))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string line = streamReader.ReadLine();

                        if (line.Contains("<Content>"))
                        {
                            //read second line to cehck tyep
                            string line2 = streamReader.ReadLine();

                            if (line2.Contains("LegalInformation"))
                            {
                                ignoreLines = true;
                            }
                            else
                            {
                                fileContent.Add(line);
                                fileContent.Add(line2);
                            }
                        }
                        else if (ignoreLines && line.Contains("</Content>"))
                        {
                            ignoreLines = false;
                        }
                        else
                        {
                            fileContent.Add(line);
                        }
                    }
                }

                string fileContentAsString = string.Join(Environment.NewLine, fileContent.ToArray());
                using (StreamWriter streamWriter = new StreamWriter(cnmtFileName))
                {
                    streamWriter.Write(fileContentAsString);
                }
            }
        }

        private static void GetNCAProgramAndControlDirectory(string directoryNCA, out string programDirectory, out string controllDirectory)
        {
            programDirectory = controllDirectory = string.Empty;

            // Get all subdirectories
            string[] subdirectoryEntries = Directory.GetDirectories(directoryNCA);

            // Loop through them to see if they have any other subdirectories
            foreach (string subdirectory in subdirectoryEntries)
            {
                // only nca in name
                if (subdirectory.Contains(".nca") && !subdirectory.Contains(".cnmt."))
                {
                    if (IsProgramDirectory(subdirectory))
                    {
                        programDirectory = subdirectory;
                    }
                    else
                    {
                        controllDirectory = subdirectory;
                    }
                }
            }
        }

        private static string GetProgramNCAFileName(string directory)
        {
            string[] files = Directory.GetFiles(directory, "*.nca");

            if (files != null && files.Length > 0)
            {
                return files[0];
            }

            return string.Empty;//not found nca file
        }

        private static string GetControlNCAFileName(string directory, string programNCAFileName)
        {
            string[] files = Directory.GetFiles(directory, "*.nca");

            foreach (string file in files)
            {
                if (file != programNCAFileName)
                {
                    return file;
                }
            }

            return string.Empty;//not found nca file
        }

        private static bool IsProgramDirectory(string directory)
        {
            string file = Directory.GetFiles(directory, "main", SearchOption.AllDirectories).FirstOrDefault();

            return file != null;
        }

        private static void Log(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
            Console.WriteLine(message);
        }

        private static void CheckFileExists(string fileName)
        {
            if (!File.Exists(fileName))
            {
                Log($"Cant find file:{fileName}");
                Environment.Exit(1);
            }
        }

        private static void DeleteWorkingDirectory(string directory)
        {
            if (Directory.Exists(directory))
            {
                Directory.Delete(directory, true);
            }
        }
    }
}
