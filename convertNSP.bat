UnityNSPConvertor.exe c:\Work\Nintendo\NintendoSDK\\Tools\CommandLineTools\AuthoringTool\AuthoringTool.exe hacpack.exe HelloWorld.nsp 01004B9000490000

#HelloWorld
set TITLE_NAME=HelloWorldRelease
set TITLE_ID=01004B9000490000
set ORIGINAL_NSP_PATH="C:\Test\HelloWorld\Builds\%TITLE_NAME%.nsp"

UnityNSPConvertor.exe %NINTENDO_SDK_ROOT%\Tools\CommandLineTools\AuthoringTool\AuthoringTool.exe hacpack.exe %ORIGINAL_NSP_PATH% %TITLE_ID%

ren "NSP_out\%TITLE_ID%.nsp" %TITLE_NAME%.nsp